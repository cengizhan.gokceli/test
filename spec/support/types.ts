import { Response } from 'supertest';
import { ILocation } from '@entities/Location';
import { DirectionsResponse } from 'openrouteservice-js';

export interface IResponse extends Response {
    body: {
        resp: DirectionsResponse;
        error: string;
    };
}

export interface IReqBody {
    locations?: ILocation[];
}
