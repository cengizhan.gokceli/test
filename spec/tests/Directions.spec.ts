import supertest from 'supertest';
import StatusCodes from 'http-status-codes';
import { SuperTest, Test } from 'supertest';

import app from '@server';
import { pErr } from '@shared/functions';
import { paramMissingError, paramCountError } from '@shared/constants';
import { IReqBody, IResponse } from '../support/types';
import { DirectionsResponse } from 'openrouteservice-js';
import { DemoRoute } from './demo.route'

describe('DirectionsController', () => {

    const directionsPath = '/api/directions';
    const getAllDirectionsPath = `${directionsPath}/all`;
    const { BAD_REQUEST, CREATED, OK } = StatusCodes;
    let agent: SuperTest<Test>;

    let demoRoute: DemoRoute;

    beforeEach(() => {
        demoRoute = new DemoRoute();
    });

    beforeAll((done) => {
        agent = supertest.agent(app);
        done();
    });
    describe(`"GET:${getAllDirectionsPath}"`, () => {

        it(`should return a JSON object and a status code of "${OK}" if the
            request was successful.`, (done) => {
            // Call API
            agent.get(getAllDirectionsPath)
                .end((err: Error, res: IResponse) => {
                    pErr(err);
                    expect(res.status).toBe(OK);
                    const directions = res.body.resp;
                    expect(directions).toBeInstanceOf(DirectionsResponse);
                    expect(res.body.error).toBeUndefined();
                    done();
                });
        });
    });
});
