
import { DirectionsController } from "../../src/controllers/Directions";
import { Response } from "express";
import { IRequest } from '@shared/constants';
import { RouteService } from '../../src/services/route.service'

export class DemoRoute {
    private controller = new DirectionsController(new RouteService());

    getAll(req: IRequest, res: Response) {
        return this.controller.directionsAll(req, res);
    }
}