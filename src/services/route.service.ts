import { apiKey } from '@shared/constants';
import { DirectionsQueryRequest, DirectionsResponse, Directions } from "openrouteservice-js";
import haversine, { CoordinateType, OptionType } from 'haversine';
import { injectable } from "inversify";

export interface IRouteService {
    getDirections(request: DirectionsQueryRequest): Promise<DirectionsResponse>;
    getDistance(
        startCoordinates: CoordinateType,
        endCoordinates: CoordinateType,
        options: OptionType): number;
}

@injectable()
class RouteService implements IRouteService {
    public async getDirections(request: DirectionsQueryRequest)
        : Promise<DirectionsResponse> {
        const directions = new Directions({
            api_key: apiKey
        });
        return await directions.calculate(request);
    }

    public getDistance(
        startCoordinates: CoordinateType,
        endCoordinates: CoordinateType,
        options: OptionType): number {
        return haversine(startCoordinates, endCoordinates, options);
    }
}

export { RouteService };
