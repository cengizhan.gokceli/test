export interface IDistance {
    distance: number;
    unit: string;
    definition: string;
}

class Distance implements IDistance {

    public distance: number;
    public unit: string;
    public definition: string;

    constructor(distance: number, unit: string, definition: string) {
        this.distance = distance;
        this.unit = unit;
        this.definition = definition;
    }
}

export default Distance;
