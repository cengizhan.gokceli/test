export interface ILocation {
    latitude: number;
    longitude: number;
    poiName: string;
}

class Location implements ILocation {

    public latitude: number;
    public longitude: number;
    public poiName: string;

    constructor(latitude: number, longitude: number, poiName: string) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.poiName = poiName;
    }
}

export default Location;
