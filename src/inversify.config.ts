import { Container } from "inversify";
import { TYPES } from "./types/types";
import { IRouteService, RouteService } from "./services/route.service";

const container = new Container();
container.bind<IRouteService>(TYPES.IRouteService).to(RouteService);

export { container };
