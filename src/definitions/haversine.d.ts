declare module 'haversine' {
    type NumberTuple = [number, number]
    type FormatType = '[lat,lon]' | '[lon,lat]' | '{lat,lng}' | 'geojson';
    type UnitType = 'km' | 'mile' | 'meter' | 'nmi';
    export type CoordinateType = NumberTuple | { lon: number, lat: number }
        | { geometry: { coordinates: NumberTuple } };
    export type OptionType = { unit?: UnitType, format: FormatType, threshold?: number }?;

    let haversine: (startCoordinates: CoordinateType,
        endCoordinates: CoordinateType,
        options?: OptionType) => number;

    export default haversine
}
