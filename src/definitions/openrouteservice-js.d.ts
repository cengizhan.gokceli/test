declare module 'openrouteservice-js' {
    type ProfileType = 'driving-car' | 'driving-hgv' | 'foot-walking' | 'foot-hiking'
        | 'cycling-regular' | 'cycling-road' | 'cycling-mountain'
        | 'cycling-electric' | 'wheelchair';
    type UnitType = 'm' | 'km' | 'mi';
    type PreferenceType = 'fastest' | 'shortest' | 'recommended';
    type FormatType = 'text' | 'html';

    interface DirectionsQueryBase {
        format: 'json';
        instructions: true;
        elevation: false;
        geometry_simplify: true;
        language: 'en-us';
        maneuvers: false;
        geometry: true;
        roundabout_exits: false;
        suppress_warnings: false;
        maximum_speed?: number;
    }

    export interface DirectionsQueryRequest implements DirectionsQueryBase {
        units: UnitType;
        preference: PreferenceType;
        instructions_format: FormatType;
        coordinates: GeoJSON.Position[];
        profile: ProfileType;
    }

    interface SummaryBase {
        distance: number;
        duration: number;
    }

    interface InfoBase {
        bbox: GeoJSON.BBox
    }

    interface StepsInfo extends SummaryBase {
        type: number;
        instruction: string;
        name: string;
        waypoints: GeoJSON.Position;
    }

    interface SegmentInfo extends SummaryBase {
        steps: StepsInfo[];
    }

    interface RoutesInfo extends InfoBase {
        summary: SummaryBase;
        segments: SegmentsInfo[];
        waypoints: GeoJSON.Position;
        geometry: string;
    }

    interface MetadataInfo {
        attribution: string;
        engine: {
            version: string;
            build_date: string;
            graph_date: string;
        };
        service: 'directions';
        query: DirectionsQueryRequest;
        timestamp: number;
    }

    export class DirectionsResponse extends InfoBase {
        metadata: MetadataInfo;
        routes: RoutesInfo[]
    }

    export class Directions {
        constructor(args: DirectionConstructorOptions);
        calculate: (settings: DirectionsQueryRequest) => Promise<DirectionsResponse>;
    }
}
