import './pre-start';
import "reflect-metadata";
import app from '@server';
import logger from '@shared/Logger';
import { port } from '@shared/constants';
import { InversifyExpressServer } from 'inversify-express-utils';
import { container } from './inversify.config';
import "./controllers/Directions";
import "./controllers/Distance";

const server = new InversifyExpressServer(container, null, null, app);
const wrappedApp = server.build();
wrappedApp.listen(port, () => {
    logger.info('Express server started on port: ' + port);
});
