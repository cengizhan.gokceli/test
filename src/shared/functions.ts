import logger from './Logger';
import { Request, Response, NextFunction } from 'express';
import StatusCodes from 'http-status-codes';

const { INTERNAL_SERVER_ERROR } = StatusCodes;

export const pErr = (err: Error) => {
    if (err) {
        logger.err(err);
    }
};

export const errorHandler = (err: Error, req: Request, res: Response, next: NextFunction) => {
    pErr(err);
    if (res.headersSent) {
        return next(err);
    }
    return res.status(INTERNAL_SERVER_ERROR).json({ error: err.message });
};
