import { Request } from 'express';
import { ILocation } from '@entities/Location';

export const paramMissingError = 'One or more of the required parameters was missing.';

export const paramCountError = 'Please provide more than one location.';

export const apiKey = '5b3ce3597851110001cf62486772f3490b924544a0f16bcee3b813a0';

export const port = 4000;

export interface IRequest extends Request {
    body: {
        locations: ILocation[];
    }
} 
