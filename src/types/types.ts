const TYPES = {
    IRouteService: Symbol.for('IRouteService')
};

const SERVICES = {
    RouteService: Symbol.for('RouteService')
}

export { TYPES, SERVICES };
