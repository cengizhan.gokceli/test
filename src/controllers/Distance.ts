import { IRouteService } from '../services/route.service';
import StatusCodes from 'http-status-codes';
import { Response } from 'express';
import fs from 'fs-extra';
import path from 'path';

import { ILocation } from '@entities/Location';
import Distance from '@entities/Distance';
import { paramMissingError, paramCountError, IRequest } from '@shared/constants';

import {
    interfaces, controller,
    httpGet, httpPost,
    request, response
} from "inversify-express-utils";
import { inject, named } from "inversify";
import { TYPES, SERVICES } from '../types/types'

const { BAD_REQUEST, OK } = StatusCodes;

@controller("/api/distance")
export class DistanceController implements interfaces.Controller {

    constructor(@inject(TYPES.IRouteService) @named(SERVICES.RouteService)
    private _routeService: IRouteService) { }

    /******************************************************************************
     *                      Get Distance - "POST /api/distance"
     ******************************************************************************/
    @httpPost("/")
    public distance(@request() req: IRequest, @response() res: Response) {
        const locations: ILocation[] = req.body.locations;
        if (!req.body || !locations || locations.length < 1) {
            return res.status(BAD_REQUEST).json({
                error: paramMissingError
            });
        }
        if (!(locations.length >= 2)) {
            return res.status(BAD_REQUEST).json({
                error: paramCountError
            });
        }
        const result = new Array<Distance>();
        locations.reduce((accumulator, currentValue, currentIndex, array) => {
            const distance = this._routeService.getDistance(
                [accumulator.longitude, accumulator.latitude],
                [currentValue.longitude, currentValue.latitude],
                { unit: 'meter', format: '[lon,lat]' });
            result.push(new Distance(
                distance,
                'meters',
                `FROM Poi ${currentIndex} TO Poi ${currentIndex + 1}`));
            return array[currentIndex];
        });
        return res.status(OK).json({ result });
    }


    /******************************************************************************
     *                      Get All Distances - "GET /api/distance/all"
     ******************************************************************************/
    @httpGet("/all")
    public distanceAll(@request() req: IRequest, @response() res: Response) {
        const raw = fs.readFileSync(path.resolve(__dirname, "../data.json"));
        const locations: ILocation[] = JSON.parse(raw.toString());
        const result = new Array<Distance>();
        locations.reduce((accumulator, currentValue, currentIndex, array) => {
            const distance = this._routeService.getDistance(
                [accumulator.longitude, accumulator.latitude],
                [currentValue.longitude, currentValue.latitude],
                { unit: 'meter', format: '[lon,lat]' });
            result.push(new Distance(
                distance,
                'meters',
                `FROM ${accumulator.poiName} TO ${currentValue.poiName}`));
            return array[currentIndex];
        });
        return res.status(OK).json({ result });
    }
}
