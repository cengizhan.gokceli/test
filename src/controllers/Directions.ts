import { IRouteService } from '../services/route.service';
import StatusCodes from 'http-status-codes';
import { Response } from 'express';
import fs from 'fs-extra'
import path from 'path'

import { ILocation } from '@entities/Location'
import { paramMissingError, paramCountError, IRequest } from '@shared/constants';

import { DirectionsQueryRequest } from "openrouteservice-js";
import {
    interfaces, controller,
    httpGet, httpPost,
    request, response
} from "inversify-express-utils";
import { inject, named } from "inversify";
import { TYPES, SERVICES } from '../types/types'

const { BAD_REQUEST, OK } = StatusCodes;

@controller("/api/directions")
export class DirectionsController implements interfaces.Controller {

    constructor(@inject(TYPES.IRouteService) @named(SERVICES.RouteService)
    private _routeService: IRouteService) { }

    /******************************************************************************
     *                      Get Directions - "POST /api/directions"
     ******************************************************************************/
    @httpPost("/")
    public async directions(@request() req: IRequest, @response() res: Response) {
        const locations: ILocation[] = req.body.locations;
        if (!req.body || !locations || locations.length < 1) {
            return res.status(BAD_REQUEST).json({
                error: paramMissingError
            });
        }
        if (!(locations.length >= 2)) {
            return res.status(BAD_REQUEST).json({
                error: paramCountError
            });
        }
        const routeRequest: DirectionsQueryRequest = {
            coordinates: locations.map(x => [x.longitude, x.latitude]),
            preference: 'recommended',
            units: 'm',
            profile: 'driving-car',
            instructions_format: 'text'
        };
        await this._routeService.getDirections(routeRequest).then(resp => {
            return res.status(OK).json({ resp });
        }, (resp) => {
            return res.status(BAD_REQUEST).json({ resp });
        });
    }


    /******************************************************************************
     *                      Get All Directions - "GET /api/directions/all"
     ******************************************************************************/
    @httpGet("/all")
    public async directionsAll(@request() req: IRequest, @response() res: Response) {
        const raw = fs.readFileSync(path.resolve(__dirname, "../data.json"));
        const data: ILocation[] = JSON.parse(raw.toString());
        const paths = data.map(x => [x.longitude, x.latitude]);
        const routeRequest: DirectionsQueryRequest = {
            coordinates: paths,
            preference: 'recommended',
            units: 'm',
            profile: 'driving-car',
            instructions_format: 'text'
        };
        await this._routeService.getDirections(routeRequest).then(resp => {
            return res.status(OK).json({ resp });
        }, (resp) => {
            return res.status(BAD_REQUEST).json({ resp });
        });
    }
}
