## DEVELOPMENT
npm run "start:dev"

## TEST
*tests are incomplete*

npm run test

## LINT
npm run lint

## PRODUCTION
npm run build & npm start

## DOCKER
docker-compose up --build

*compose dosyasından port değiştirilebilir*

# ENDPOINTS
- http://localhost:4000/api/directions (POST)

örnek body;
{
    "locations": [
{
        "latitude": 40.978696156559884, 
        "longitude": 29.13730406235724
},
{
        "latitude": 40.97263724900384, 
        "longitude": 29.141359562598126
}
    ]
}

İki ya da daha fazla location objesi gönderildiğinde direction'ları JSON formatında döner

- http://localhost:4000/api/directions/all (GET)

Lokal data.json'dan okuyarak direction'ları JSON formatında döner
